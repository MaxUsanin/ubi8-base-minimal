## ubi8-base-minimal lightweight container
 
Container uses `registry.access.redhat.com/ubi8/ubi-minimal:latest` as base, with extended microdnf repository

 [Official page](https://www.redhat.com/en/blog/introducing-red-hat-universal-base-image)
 
 [Images](https://hub.docker.com/repository/docker/usanin/ubi8-base-minimal/tags?page=1)
