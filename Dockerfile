FROM registry.access.redhat.com/ubi8/ubi-minimal:latest
LABEL maintainer="Usanin Max usanin.max@gmail.com"
RUN rm -rf /etc/yum.repos.d/*
COPY ubi.repo /etc/yum.repos.d

RUN set -xe \
    && cat /etc/yum.repos.d/ubi.repo \
    && microdnf -y install findutils